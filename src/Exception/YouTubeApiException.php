<?php

namespace Drupal\feeds_youtube_api\Exception;

use RuntimeException;

/**
 * Base class for YouTube API runtime exceptions.
 */
class YouTubeApiException extends RuntimeException {}
